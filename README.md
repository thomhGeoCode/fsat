# Readme for FSAT

## Fracture Surface Analysis Toolbox
Thank you very much for your interest in FSAT.
FSAT is a MATLAB toolbox to analyze single profiles as well as fracture surfaces with respect to their morphology and roughness.
There is a youtube playlist explaining how to use FSAT and showing its capabilities: https://www.youtube.com/playlist?list=PLnUwX1xmEVr8fP_WDJeac7rKQWkSOPsTX

## Installing FSAT
Download FSAT and unzip the folder.
FSAT requires MATLAB. It has been tested with Matlab 2020b.
The used export functionality for figures requires at least Matlab 2020a. Any newer version will be fine. 
The roughness analysis itsel should work with older versions but figure export will not be possible for any Matlab older than Matlab 2020a.
There are no further requirements.

## Preparing your raw data
FSAT needs your raw data in ASCII file format in which coordinates are organized in columns.
In a height profile, the first column is read as distance along the profile (x) and the second column as height information corresponding to the respective x-value.
In a surface, the first column is read as x-direction, the second value as y-direction and the third one as height value corresponding to those x-y-coordinate.
If you obtain your surface from a 3D scanner, your data will most likely already be stored in such a way.

## Setting the Metadata
Your first step using FSAT is preparing the metadata file.
See generateMetadata.m as an example.
FSAT separates between single profiles (case a), single surfaces (case b), two profiles forming a fracture as top and bottom (case c) and two surfaces forming a fracture as top and bottom (case d).
Depending on the case, different kind of metadata is required.
See the in-code documentation in generateMetadata.m for further information.
Note that your paths given need to match your current operating system. 
Examples provided with FSAT are based on a Linux/Unix system.
Save your metadata in a .mat file in a Matlab structure called sample.
You can combine multiple analyses in one sample by adding items to the structure.
FSAT will automatically iterate through all items provided in the structure sample.

## Running the analysis
You can start FSAT using the analyzesample.m skript.
You only need to provide the path to the previously generated metadata file.
Everything else will go from here directly.

## Further Information
Take a look at our publication for further details such as the automatically generated output, code verification, proposed workflow using FSAT and the theoretical background behind FSAT.

## License
Along with this file and the source code of FSAT you should have received a version of our license.

## Citation
Please give credit by referencing our corresponding work: 
Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
doi: 10.1016/j.compgeo.2020.103997. 
https://doi.org/10.1016/j.compgeo.2020.103997

Copyright (c) 2020, 2021 Thomas Heinze

This file was written by Thomas Heinze, Bochum, Germany, 2020,2021 as part of the FSA Toolbox.
