%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function splits the metadata provided for the two profiles which 
% form top and bottom of the fracture and passes it on to the separate 
% analysis of each profile. The results of each profile are combined and 
% mean values as well as divergences are calculated and stored in a csv 
% file in the corresponding directory. Additionally to the analysis of the 
% single profile, the aperture distribution of the fracture is analyzed.
% For future analysis and modeling the workspace is stored as a mat-file.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function roughnessFracture2d(structure)

  % save pathResults because it will be overwritten
  resultspath = structure.pathResults;

  %% Top
  % overwrite fields for top side
  structure.pathRawdata = structure.pathRawdataTop;
  structure.pathResults = [structure.pathResults,'top/'];
  
  % Make subfolder if not yet present
  if ~exist(structure.pathResults, 'dir')
    mkdir(structure.pathResults);
  end

  GJHtop = roughnessProfile(structure);

  %% Bottom
  % Overwrite fields for bottom side
  structure.pathRawdata = structure.pathRawdataBottom;
  structure.pathResults = [resultspath,'bottom/'];

  % Make subfolder if not yet present
  if ~exist(structure.pathResults, 'dir')
    mkdir(structure.pathResults);
  end

  GJHbottom = roughnessProfile(structure);

  structure.pathResults = resultspath;

  clear resultspath

  %% Joined analysis
  % Remove identical RowNames to combine both tables
  GJHtop.Properties.RowNames={};
  GJHbottom.Properties.RowNames={};
  
  % Combine values of both surfaces assuming self-affinity or at least a
  % strong similarity between both. Check the results and the provided
  % difference of each value provided therein.
  results = [GJHtop; GJHbottom];
  clear GJHtop GJHbottom
  
  % Calculate Difference
  results{3,:} = results{1,:} - results{2,:};
  
  % Calculate Mean
  results{4,:} = (results{1,:} + results{2,:}) * 0.5;
  
  % Update RowNames
  results.Properties.RowNames={'top',  'bottom', 'diff', 'mean'};
  
  % export table
  writetable(results,strcat(structure.pathResults,'results.csv'),...
    'WriteRowNames',true)

  %% Aperture analysis
  calculateAperture(structure);
  
  %% Save workspace
  save([structure.pathResults, 'workspace'],'structure','results')

end
