%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the complete set of parameters to describe the
% roughness of a profile implemented in the FSA toolbox. It further handles
% the import of the rawdata and preprocessing adjusting the coordinate 
% system, cutting the boundaries, interpolating a smooth line to remove 
% macroscopic tortuosity, and interpolates the rawdata onto a regular 
% horizontal grid for further analysis.
% Calculation of roughness parameters is done by calling the corresponding
% functions (calculateGaussian, calculateJRC, calculateHurstExp). 
% The results of each function are collected in a single csv output file 
% saved in the directory specified in the metadata (pathResults) and is 
% named results.csv.
% This function further generates one figure showing the interpolated,
% relative height data on which the subsequent analysis is based.
% The generated file is named as:
% - relativeHeight
% For future analysis and modeling, the workspace is saved in a mat-file.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [results] = roughnessProfile(structure)

  structure.data = importRawdata(structure);

  structure.data = setToZero(structure);

  x0 = structure.cutoffx0;
  xend = structure.cutoffxend;

  structure.xUniform = transpose(x0:structure.spacing:xend);

  structure.zPlane = interpPlane(structure);

  structure.zUniform = interpUniformGrid(structure);

  % Calculate relative height by subtracting the interpolated line 
  % structure with the macroscopic tortuosity from the interpolated uniform
  % data.
  structure.relHeight = structure.zUniform - structure.zPlane;

  if (strcmp(structure.figures,'yes'))
    figure()
    plot(structure.xUniform,structure.relHeight)
    axis tight
    xlabel(['x-direction [', structure.units,']'])
    ylabel(['relative height [', structure.units,']'])
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'relativeHeight.', ...
      structure.figfileformat], 'Resolution', 300)
  end
  
  % Calculate the mean height along the profile
  % NOTE: This is only done for parallelism with the handling of a surface
  % as for a profile the mean height after interpolation is zero within the
  % computational accuracy.
  structure.meanHeight = mean(structure.relHeight,1);

  % Calculate derivation from mean height
  % NOTE: This is only done for parallelism with a surface because the
  % mean height after interpolation and subtraction of the macroscopic
  % tortuosity is zero within computational accuracy. Therefore
  % .devFromMean is eqal to relHeight.
  structure.devFromMean = structure.relHeight - structure.meanHeight;

  %% Calculate relative height statistics (=derivation from the mean)
  statistics = calculateRelHeightStatistics(structure);
  
  %% Spatial and amplitude measures
  spatialAmplitude = calculateSpatialAmplitude(structure,1);

  %% Joint Rougness Coefficient
  jrc = calculateJRC(structure,1);

  %% Fractal Dimension
  hurst = calculateHurstExp(structure, 1);

  %% Generate output
  S = struct2table(statistics);
  G = struct2table(spatialAmplitude);
  J = struct2table(jrc);
  H = struct2table(hurst);

  results = [S,G,J,H];

  % export table
  writetable(results,strcat(structure.pathResults,'results.csv'));

  %% close all figures
  close all;
  
  %% save workspace
  clear S G J H spatialAmplitude jrc hurst statistics
  save([structure.pathResults,'workspace'],'structure','results');

end
